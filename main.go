package main

import (
	"fmt"
	"os"
	"path/filepath"

	tea "github.com/charmbracelet/bubbletea"
)

var fileName = ""
var title = ""

func main() {

	if len(os.Args[1:]) != 1 {
		fmt.Printf("Please provide a file name")
		os.Exit(1)
	}

	fileName = os.Args[1]
	title = filepath.Base(fileName)

	p := tea.NewProgram(initialModel())
	if _, err := p.Run(); err != nil {
		fmt.Printf("Alas, there's been an error: %v", err)
		os.Exit(1)
	}
}
