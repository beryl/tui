# Beryl TUI
> A Text User Interface for the Beryl Todo list format

# Install
```
go install codeberg.org/beryl/goi
```

# Usage
```
beryl-tui filename.md
```
press `?` to get a list of commands

See [beryl-go](https://codeberg.org/beryl/go) for full spec.
See [here](https://codeberg.org/beryl/tui/-/blob/main/todo.md) for the roadmap
